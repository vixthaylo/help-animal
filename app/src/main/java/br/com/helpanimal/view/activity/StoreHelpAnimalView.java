package br.com.helpanimal.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import br.com.helpanimal.R;
import br.com.helpanimal.view.adapter.ListMenuAdapter;
import br.com.helpanimal.view.adapter.ListStoreAdapter;

public class StoreHelpAnimalView extends AppCompatActivity {

    private GridLayoutManager lLayout;
    private RecyclerView rView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_help_animal);
        lLayout = new GridLayoutManager(this, 1);
        rView = (RecyclerView) findViewById(R.id.rclView);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        ListStoreAdapter rcAdapter = new ListStoreAdapter(this,getIntent().getStringExtra("title"), getIntent().getIntExtra("img",R.drawable.banner4));
        rView.setAdapter(rcAdapter);

        ((ImageButton)findViewById(R.id.btninicio)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ((TextView)findViewById(R.id.txtStore)).setText(getIntent().getStringExtra("title"));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
