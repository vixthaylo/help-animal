package br.com.helpanimal.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import br.com.helpanimal.R;
import br.com.helpanimal.view.adapter.ListMenuAdapter;

public class ListMenuHalpAnimalView extends AppCompatActivity {


    private GridLayoutManager lLayout;
    private RecyclerView rView;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    RelativeLayout relativeLayout;
    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_menu_help_animal);

        permissionSdCard();

        lLayout = new GridLayoutManager(this, 1);

        rView = (RecyclerView) findViewById(R.id.recyclerView);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        ListMenuAdapter rcAdapter = new ListMenuAdapter(this);
        rView.setAdapter(rcAdapter);

    }

    public void listmenuClick(View item) {


        switch (item.getId()){
            case R.id.btninicio:
                startActivity(new Intent(this, MenuHelpAnimalView.class));
                
                break;
        }
    }


    @SuppressLint("LongLogTag")
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {

        Log.e("onRequestPermissionsResult", String.valueOf(requestCode));
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, "Permissão garantida!", Toast.LENGTH_SHORT).show();
                } 
        }
    }

    private void permissionSdCard() {


        // Se não possui permissão
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            int writePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int readPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

            if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    this.requestPermissions(PERMISSIONS_STORAGE, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                }
            }

        }
    }



}
