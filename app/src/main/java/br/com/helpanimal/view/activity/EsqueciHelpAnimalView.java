package br.com.helpanimal.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.helpanimal.R;

public class EsqueciHelpAnimalView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.esqueci_help_animal);
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,LoginHelpAnimalView.class));
    }

    public void esqueciClick(View item) {
        onBackPressed();

    }
}
