package br.com.helpanimal.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.helpanimal.R;
import br.com.helpanimal.view.activity.DetailStoreHelpAnimalView;
import br.com.helpanimal.view.activity.StoreHelpAnimalView;


/**
 * Created by Julierlem on 12/08/17.
 */

public class ListStoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Context mContext;
    private int activity;
    List<String> liststr;
    private final int DEFAULT_VIEW_TYPE = 1;
    private final int NATIVE_AD_VIEW_TYPE = 2;
    private String[] list_string = new String[]{"Adestradores", "Clínicas", "Hospedagens", "Hospital", "Passeadores", "Pets shops", "Veterinários", "Adoações",""};
    private int[] list_img = new int[]{R.drawable.banner4d, R.drawable.banner4, R.drawable.banner4e, R.drawable.banner4f,
            R.drawable.banner4c,R.drawable.banner4a, R.drawable.banner4b, R.drawable.banner5b, R.drawable.banner4};
    private String name;
    private int img;



    public ListStoreAdapter(Context context, String name, int img){

        this.name = name;
        this.img = img;
        this.mContext = context;

    }

    @Override
    public int getItemViewType(int position) {

        if(position == 8)
            return NATIVE_AD_VIEW_TYPE;
        
        return DEFAULT_VIEW_TYPE;
    }

    /**
     * Verificando a conxeção
     **/
    public boolean isOnline() {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;

        } else {
            conectado = false;
        }


        return conectado;
    }





    public class ViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;
        public TextView text;
        public LinearLayout linear;


        public ViewHolder(View view) {
            super(view);
            text = (TextView) view.findViewById(R.id.txtname);
            linear = (LinearLayout) view.findViewById(R.id.linear);
            cardView = (CardView) view.findViewById(R.id.card_view);

        }


    }


    public class FooterViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;
        public ImageButton btnface, btninst, btnsite;


        public FooterViewHolder(View view) {
            super(view);
            btnface = view.findViewById(R.id.btnface);
            btninst = view.findViewById(R.id.btninst);
            btnsite = view.findViewById(R.id.btnsite);
            cardView = (CardView) view.findViewById(R.id.card_view);

        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {



        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        if (viewType == DEFAULT_VIEW_TYPE) {
            view = layoutInflater
                    .inflate(R.layout.item_cardview, parent, false);
            CardView card = (CardView)view.findViewById(R.id.card_view);
            card.setMaxCardElevation(5.0F);
            card.setRadius(15.0F);

            return new ViewHolder(view);
        }else if (viewType == NATIVE_AD_VIEW_TYPE){
            view = layoutInflater
                    .inflate(R.layout.item_footer_default_cardview, parent, false);
            CardView card = (CardView)view.findViewById(R.id.card_view);

            return new FooterViewHolder(view);
        }else{

            view = layoutInflater
                    .inflate(R.layout.item_cardview, parent, false);
            CardView card = (CardView)view.findViewById(R.id.card_view);
            card.setMaxCardElevation(5.0F);
            card.setRadius(15.0F);
            return new ViewHolder(view);
            
        }

       
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position)  {


        if (!(viewHolder instanceof FooterViewHolder)) {
            final ViewHolder holder = (ViewHolder) (ViewHolder) viewHolder;
            holder.linear.setBackgroundResource(img);
            holder.text.setText(name);
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(mContext, DetailStoreHelpAnimalView.class);
                    it.putExtra("title", holder.text.getText());
                    it.putExtra("img",img);
                    mContext.startActivity(it);

                }
            });

        }




    }




    @Override
    public int getItemCount() {

        return list_string.length;
    }





}