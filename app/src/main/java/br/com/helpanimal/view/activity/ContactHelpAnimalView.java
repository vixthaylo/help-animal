package br.com.helpanimal.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.helpanimal.R;

public class ContactHelpAnimalView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_help_animal);
    }

    public void contactClick(View item) {

        switch (item.getId()){

            case R.id.btninicio:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,MenuHelpAnimalView.class));
    }
}
