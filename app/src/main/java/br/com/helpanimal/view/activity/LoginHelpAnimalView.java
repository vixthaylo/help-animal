package br.com.helpanimal.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.helpanimal.R;

public class LoginHelpAnimalView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_help_animal);



    }

    public void loginClick(View item) {

        switch (item.getId()){

            case R.id.btnsend:
                startActivity(new Intent(LoginHelpAnimalView.this, ListMenuHalpAnimalView.class));
                finish();
                break;

            case R.id.btncadastro:
                startActivity(new Intent(LoginHelpAnimalView.this, CadastroHelpAnimalView.class));
                finish();
                break;

            case R.id.btnesqueci:
                startActivity(new Intent(LoginHelpAnimalView.this, EsqueciHelpAnimalView.class));
                finish();
                break;


        }

    }


    public void cadastraClick(View view) {


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, LoginHelpAnimalView.class));
    }
}
