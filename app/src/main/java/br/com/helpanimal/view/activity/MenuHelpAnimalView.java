package br.com.helpanimal.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import br.com.helpanimal.R;

public class MenuHelpAnimalView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_help_animal);
    }

    public void menuClick(View item) {

        switch (item.getId()){

            case R.id.btnclosemenu:
               onBackPressed();
                break;

            case R.id.btninicio:
                onBackPressed();
                break;

            case R.id.btnmyconta:

                if(((LinearLayout)findViewById(R.id.linear)).getVisibility() == View.GONE)
                    ((LinearLayout)findViewById(R.id.linear)).setVisibility(View.VISIBLE);
                else
                    ((LinearLayout)findViewById(R.id.linear)).setVisibility(View.GONE);

                break;


            case R.id.btnmapa:
                startActivity(new Intent(this, MapsHelpAnimilView.class));
                break;

            case R.id.btndado:
                startActivity(new Intent(this, MeuDadoHelpAnimalView.class));
                break;




            case R.id.btncontato:
                break;

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
