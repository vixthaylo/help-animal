package br.com.helpanimal.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import br.com.helpanimal.R;

public class SplashHelpAnamilView extends AppCompatActivity {
    // Remove the below line after defining your own ad unit ID.

    private static final int WAIT_TIME = 6000;
    private Timer waitTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        waitTimer = new Timer();
        waitTimer.schedule(new TimerTask() {

            @Override
            public void run() {

                SplashHelpAnamilView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        startActivity(new Intent(SplashHelpAnamilView.this, LoginHelpAnimalView.class));
                        finish();
                    }
                });
            }
        }, WAIT_TIME);


    }




}
