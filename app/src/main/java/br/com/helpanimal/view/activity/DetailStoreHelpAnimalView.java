package br.com.helpanimal.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.helpanimal.R;
import br.com.helpanimal.view.adapter.ListMenuAdapter;
import br.com.helpanimal.view.adapter.ListStoreItemAdapter;

public class DetailStoreHelpAnimalView extends AppCompatActivity {

    private GridLayoutManager lLayout;
    private RecyclerView rView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_store_help_animal);

        ((TextView)findViewById(R.id.txtnamelocal)).setText(getIntent().getStringExtra("title"));

        ((LinearLayout)findViewById(R.id.linear)).setBackgroundResource(getIntent().getIntExtra("img",R.drawable.banner4));
        lLayout = new GridLayoutManager(this, 1);

        rView = (RecyclerView) findViewById(R.id.recyclerView);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        ListStoreItemAdapter rcAdapter = new ListStoreItemAdapter(this,getIntent().getStringExtra("title"), getIntent().getIntExtra("img",R.drawable.banner4));
        rView.setAdapter(rcAdapter);
        /*((ImageButton)findViewById(R.id.btninicio)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/


        ((LinearLayout)findViewById(R.id.linear)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(DetailStoreHelpAnimalView.this, ContactStoreHelpAnimalView.class);
                it.putExtra("store",getIntent().getStringExtra("title"));
                startActivity(it);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
