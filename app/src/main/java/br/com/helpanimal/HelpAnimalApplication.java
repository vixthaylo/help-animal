package br.com.helpanimal;

import android.app.Application;
import android.support.multidex.MultiDex;

/**
 * Created by julierlem on 28/02/2018.
 */

public class HelpAnimalApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
    }
}
